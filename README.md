# Personal Website

A personal website showcasing my achievements. _Very_ WIP.
View it live at [maxwason.com](https://www.maxwason.com)

---

#### More Details

##### Tech Stack
* Web Tech:
    * Vue.js - Main JS Framework
        * Vuex - State management
        * vue-router - SPA (Single Page Application) routing
        * vue-loader - Single file .vue templates
    * Quasar - Main UI Framework
    * Axios - Async calls
    * Helpers
        * Babel - ES6 Syntax
        * ESLint - Javascript linting, AirBNB Style
        * Webpack - Environment setup/compilation
        * Production version optimized with Uglify.js, html-minifier, and cssnano
        * ToDo: Add prerender-spa plugin
* Additional Tools:
    * IntelliJ IDEA Ultimate - IDE
    * GitKraken - Git client
    * GitLab - Repository hosting + runner (for CD + )
    * Google domains - Domain registry
    * Netlify - Hosting
